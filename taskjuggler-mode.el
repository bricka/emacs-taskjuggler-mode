;;; taskjuggler-mode.el --- A mode for editing TaskJuggler files

;; Author: Alex Figl-Brick <alex@alexbrick.me>
;; Version: 0.1
;; Package-Requires: ((emacs "24.3") (tree-sitter) (tree-sitter-indent))
;; URL: https://gitlab.com/bricka/emacs-taskjuggler-mode

;;; Commentary:

;; This package adds suport for TaskJuggler files. It provides syntax
;; highlighting and indentation based on tree-sitter grammars.

;;; Code:

(require 'tree-sitter-hl)
(require 'tree-sitter-indent)

(defvar taskjuggler-mode-syntax-table
  (let ((st (make-syntax-table)))
    (modify-syntax-entry ?# "<" st)
    (modify-syntax-entry ?\n ">" st)
    st))

(defvar-keymap taskjuggler-mode-map
  :doc "Keymap for `taskjuggler-mode'"
  :parent prog-mode-map
    "C-c C-c" #'taskjuggler-build)

(defconst taskjuggler-mode-tree-sitter-patterns
  [
(source_file (attribute (attribute_name name: (identifier) @keyword)))

(attribute_name name: (identifier) @attribute)
(attribute_name scenario: (identifier) @variable.parameter)
(new_thing_name) @keyword

[(single_quote_string) (double_quote_string)] @string
(multiline_string (multiline_string_start) @punctuation.special (multiline_string_end) @punctuation.special) @string
(number) @number

(relative_identifier "!" @punctuation.special)

(comment) @comment
])

(defvar taskjuggler-indent-offset 4)

(defconst tree-sitter-indent-taskjuggler-scopes
  '((indent-body . (attribute)))
  "Scopes for indenting with tree-sitter.")

(defun taskjuggler-build ()
  "Build the current Taskjuggler file."
  (interactive)
  (compile (concat "tj3 '" (buffer-file-name) "'")))

(define-derived-mode taskjuggler-mode prog-mode "Taskjuggler"
  "Major mode for editing TaskJuggler files.

\\{taskjuggler-mode-map}"
  ;; Indentation
  (setq-local indent-line-function #'tree-sitter-indent-line)

  ;; Comments
  (setq-local comment-start "#")
  (setq-local comment-start-skip "^\\s*#\\(?: \\|$\\)")
  (setq-local comment-use-syntax nil)

  ;; Syntax Highlighting
  (setq-local tree-sitter-hl-default-patterns taskjuggler-mode-tree-sitter-patterns)

  ;; Compile
  (setq-local compile-command (concat "tjp3 " (buffer-file-name)))

  (tree-sitter-hl-mode)
  (tree-sitter-indent-mode))

(add-to-list 'auto-mode-alist '("\\.tjp\\'" . taskjuggler-mode))
(add-to-list 'tree-sitter-major-mode-language-alist '(taskjuggler-mode . taskjuggler))

(provide 'taskjuggler-mode)
;;; taskjuggler-mode.el ends here
